package com.devcamp.j04_javabasic.s10;

public class Subtask2 extends CCat implements IFlyable {

    // Tạo ra con mèo biết bay:
    @Override
    public void fly() {
        // TODO Auto-generated method stub
        System.out.println("con mèo đang bay!");
    }

    public static void main(String[] args) {
        Subtask2 subtask2 = new Subtask2();
        subtask2.fly();
        subtask2.animalSound();
    }
    
}
